const hash: any = {};

window.location.hash
    .replace(/^#\/?/, '')
    .split('&')
    .forEach(function (keyValue: string) {
        const splitIndex: number = keyValue.indexOf('=');
        if (splitIndex != -1) {
            const key: string = keyValue.substring(0, splitIndex);
            const value: string = keyValue.substring(splitIndex + 1);
            hash[key] = decodeURIComponent(value);
        }
    });

console.log('initial hash', hash);

if ('access_token' in hash) {
    window.opener.postMessage(JSON.stringify({
        type: 'access_token',
        access_token: hash['access_token'],
        expires_in: hash['expires_in'] || 0
    }), '*'); // TODO(oskopek): Add back verification.

    window.close();
}
