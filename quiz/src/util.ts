export function editDistance (s: string, t: string) : number {
    const a = s.toLowerCase();
    const b = t.toLowerCase();
    let D = Array(a.length + 1);
    for (let i = 0; i <= a.length; i++) {
        D[i] = Array(b.length + 1);
        D[i][0] = i;
    }

    for (let j = 0; j <= b.length; j++) {
        D[0][j] = j;
    }

    for (let j = 1; j <= b.length; j++) {
        for (let i = 1; i <= a.length; i++) {
            if (a[i-1] === b[j-1]) {
                D[i][j] = D[i-1][j-1];
            } else {
                D[i][j] = Math.min(
                    D[i-1][j] + 1, // delete
                    D[i][j-1] + 1, // insert
                    D[i-1][j-1] + 1 // substitution
                );
            }
        }
    }

    return D[a.length][b.length];
};

//The maximum is exclusive and the minimum is inclusive
export function randInt(min : number, max : number) : number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
};

export function stripName(name : string) : string {
    return name.replace(/\([^)]*\)/g, "").replace(/[ ]+/g, " ").trim();
}