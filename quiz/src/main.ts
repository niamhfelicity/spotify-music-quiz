import { login, SongInfo, setSpotifyApi, getSpotifyApi, isPlaying, timeoutPlay, pause, next, getSongInfo } from "./spotify";
import { editDistance } from "./util";
import * as SpotifyWebApi from "spotify-web-api-js";

const loginButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById('btn-login');

loginButton.onclick = () => {
    login((accessToken: string) : void => {
        console.log("Access token: " + accessToken);
        const spotifyApi = new SpotifyWebApi();
        spotifyApi.setAccessToken(accessToken);
        setSpotifyApi(spotifyApi)
        .then(() => {
            loginButton.hidden = true;
            welcomeText.hidden = true;
            playButton.hidden = false;
            nextButton.hidden = false;
            resultForm.hidden = false;
            playButton.focus();
        });
    });   
};

const welcomeText: HTMLButtonElement = <HTMLButtonElement>document.getElementById('welcome-text');
const playButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById('btn-play');
const nextButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById('btn-next');

const resultForm: HTMLFormElement = <HTMLFormElement>document.getElementById("result-form");
const artistField: HTMLInputElement = <HTMLInputElement>document.getElementById("artist-field");
const songField: HTMLInputElement = <HTMLInputElement>document.getElementById("song-field");
const submitButton: HTMLInputElement = <HTMLInputElement>document.getElementById("btn-submit");
const resultElement : HTMLElement = document.getElementById("result");

const showMessage = (msg : string) => {
    console.log("Showing message: " + msg);
    resultElement.textContent = msg;
};

const clearMessage = () => showMessage("");

const resetForm = () => {
    resultForm.reset();
    songField.focus();
};

const songTimeout : number = 30;

const startPlaying = () => {
    if (getSongInfo() == null) {
        moveToNext();
    }
    // clearMessage();
    timeoutPlay(songTimeout);
};

const stopPlaying = () => {
    pause();
};

const togglePlay = () => {
    if (isPlaying()) {
        stopPlaying();
    } else {
        startPlaying();
    }
};

playButton.onclick = togglePlay;

const moveToNext = () => {
    stopPlaying();
    next();
};

const moveToNextAndPlay = () => {
    moveToNext();
    startPlaying();
}

nextButton.onclick = moveToNextAndPlay;

const submit = () : void => {
    const artist = artistField.value;
    const song = songField.value;

    if (isSongInfoCorrect(new SongInfo(artist, song, null))) {
        showMessage("Yes, correct!");
    } else {
        showMessage("The real answer is: " + getSongInfo());
    }
    resetForm();
    moveToNextAndPlay();
};

declare type EnterCallback = () => void;
declare type KeyCallback = (keyEvent : KeyboardEvent) => void;
const onEnter = (callback : EnterCallback) : KeyCallback => {
    if (callback == null) {
        throw new Error("Null callback.");
    }
    return (keyEvent :KeyboardEvent) : void => {
        if (keyEvent.key === "Enter") {
            callback();
        }
    };
};

submitButton.onclick = submit;
artistField.onkeypress = onEnter(submit);
songField.onkeypress = onEnter(submit);

const isSongInfoCorrect = (songInfo : SongInfo) : boolean => {
    const goldInfo = getSongInfo();
    return isCorrect(goldInfo.artist, songInfo.artist)
     && isCorrect(goldInfo.song, songInfo.song);
};

const isCorrect = (expected: string, value: string) : boolean => {
   console.log(`Expected: ${expected}, got: ${value}, distance: ${editDistance(expected, value)}`);
   return editDistance(expected, value) < 10;
};
