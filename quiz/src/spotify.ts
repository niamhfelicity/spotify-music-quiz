/// <reference path="../node_modules/spotify-web-api-js/src/typings/spotify-web-api.d.ts" />
import * as $ from "jquery";
import { randInt, stripName } from "./util";

declare type LoginCallback = (access_token: string) => void;

export function login(callback: LoginCallback) {
    const CLIENT_ID = '9f988a27428041bd8adc7f34efdef401';
    const REDIRECT_URI = 'http://oskopek.gitlab.io/spotify-music-quiz/auth.html';
    
    function getLoginURL(scopes: Array<string>) {
        return 'https://accounts.spotify.com/authorize?client_id=' + CLIENT_ID +
            '&redirect_uri=' + encodeURIComponent(REDIRECT_URI) +
            '&scope=' + encodeURIComponent(scopes.join(' ')) +
            '&response_type=token';
    }

    const url = getLoginURL([
        'user-read-email',
        'user-library-read',
        'playlist-read-private',
        'playlist-read-collaborative',
        'user-top-read',
        'user-read-recently-played'
    ]);

    const width = 450,
        height = 730,
        left = (screen.width / 2) - (width / 2),
        top = (screen.height / 2) - (height / 2);

    window.addEventListener("message", function (event) {
        var hash = JSON.parse(event.data);
        if (hash.type == 'access_token') {
            callback(hash.access_token);
        }
    }, false);

    const w = window.open(url,
        'Spotify',
        'menubar=no,location=no,resizable=no,scrollbars=no,status=no, width=' + width + ', height=' + height + ', top=' + top + ', left=' + left
    );
};

export class SongInfo {
    artist: string;
    song: string;
    url: string;
    constructor(artist : string, song: string, url : string) {
        this.artist = artist;
        this.song = song;
        this.url = url;
    };
    toString() {
        return this.song + " by " + this.artist;
    };
};

let spotifyApi : any = null;
let trackList : Array<SongInfo>;

export function setSpotifyApi(spotify : any) : Promise<void> {
    spotifyApi = spotify;
    return spotifyApi.getMySavedTracks({limit: 50, offset: 0})
        .catch((err : any) => {
            console.log("Error occured while reading saved tracks: " + JSON.stringify(err))
        })
        .then((res : SpotifyApi.UsersSavedTracksResponse) => {
            trackList = new Array<SongInfo>();
            for (let item of res.items) {
                const song = trackToSong(item.track);
                console.log("Adding " + song);
                trackList.push(song);
            }
        });
};

export function getSpotifyApi() : any {
    return spotifyApi;
};

const audio : HTMLAudioElement = new Audio();
let currentSong : SongInfo = null;
let currentTimeout : number = null;

export function isPlaying() : boolean {
    return !audio.paused;
}

export function timeoutPlay(timeoutSeconds : number) {
    if (currentSong == null) {
        throw new Error("Current song is null.");
    }
    audio.src = currentSong.url;
    audio.play();
    console.log('Playing ' + currentSong);                    
    currentTimeout = setTimeout(pauseAudio, timeoutSeconds * 1000);
};

function pauseAudio() {
    audio.pause();
};

export function pause() {
    if (currentTimeout != null) {
        clearTimeout(currentTimeout);
        currentTimeout = null;
    }
    pauseAudio();
    console.log('Pausing ' + currentSong);                    
};

export function getSongInfo() : SongInfo {
    return currentSong;
};

function trackToSong(track : SpotifyApi.TrackObjectFull) : SongInfo {
    if (track == null) {
        throw new Error("Cannot convert null track to song.");
    }
    const artist : string = stripName(track.artists[0].name);
    const name : string = stripName(track.name);
    return new SongInfo(artist, name, track.preview_url);
};

export function next() {
    if (trackList == null) {
        throw new Error("Track list is empty.");
    }
    const nextTrackIndex : number = randInt(0, trackList.length);
    currentSong = trackList[nextTrackIndex];
};
