import { editDistance, stripName } from "../src/util";
import { expect } from "chai";
import "mocha";

describe('Edit distance', () => {
    it('should return 3 for wiki examples', () => {
        expect(editDistance("kitten", "sitting")).to.equal(3);
        expect(editDistance("sitting", "kitten")).to.equal(3);
        expect(editDistance("Sunday", "Saturday")).to.equal(3);
        expect(editDistance("Saturday", "Sunday")).to.equal(3);
    });

    it('should be case insensitive', () => {
        expect(editDistance("Sunday", "saTUrday")).to.equal(3);
        expect(editDistance("Saturday", "sunday")).to.equal(3);
    });
});


describe('Strip parenthesis', () => {
    it('returns the correct results', () => {
        expect(stripName('Airplanes (feat. Hayley Williams of Paramore) ')).to.equal('Airplanes');
        expect(stripName('(as) Airplanes (feat. Hayley (re) () (sa)')).to.equal('Airplanes');
    });
});